<?php

use Illuminate\Database\Seeder;

class userTableSeeder extends Seeder
{

    public function run(){
        factory(App\User::class, 5)->create()->each(function ($user){
           $user->profile->save(factory(App\Profile::class)->make());
        });
    }


    /**
     * Run the database seeds.
     *
     * @return void
     */
//    public function run()
//    {
//        $faker = Faker::create();
//        foreach (range(1,10) as $index) {
//            DB::table('users')->insert([
//                'profession_id' => $faker->numberBetween(1,5),
//                'name' => $faker->name,
//                'email' => $faker->email,
//                'password' => bcrypt('secret'),
//            ]);
//        }
//    }


}
