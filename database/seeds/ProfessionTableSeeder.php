<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ProfessionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $professions=[
            [
                'name'=>'Student',
                'created_at'=>Carbon::now()->toDateString(),
                'updated_at'=>Carbon::now()->toDateString()
            ],
            [
            'name'=>'Professor',
                'created_at'=>Carbon::now()->toDateString(),
                'updated_at'=>Carbon::now()->toDateString()
        ],
        [
        'name'=>'Teacher',
            'created_at'=>Carbon::now()->toDateString(),
            'updated_at'=>Carbon::now()->toDateString()
          ],
        [
            'name'=>'Programmer',
            'created_at'=>Carbon::now()->toDateString(),
            'updated_at'=>Carbon::now()->toDateString()

        ],
            [
                'name'=>'Doctor',
                'created_at'=>Carbon::now()->toDateString(),
                'updated_at'=>Carbon::now()->toDateString()
            ],
            [
            'name'=>'Engineer',
                'created_at'=>Carbon::now()->toDateString(),
                'updated_at'=>Carbon::now()->toDateString()
             ]
        ];
        DB::table('professions')->insert($professions);
        echo "ProfessionTableSeeder run successfully";
    }
}
