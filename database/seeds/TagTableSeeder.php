<?php

use Illuminate\Database\Seeder;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($i=0, $max=10; $i < $max;  $i++){
            $tag=$faker->word;
            BD::table('tags')->insert([
               'name'=>$tag,
               'slug'=>$tag,
               'created_by'=>$faker->numberBetween(1, 5),

            ]);
    }
    }
}
