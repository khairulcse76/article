<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('picture', 100)->nullable();
            $table->text('bio', 100)->nullable();
            $table->string('web', 100)->nullable();
            $table->string('facebook', 100)->nullable();
            $table->string('github', 100)->nullable();
            $table->timestamps();

            $table->foreign('user_id')->reference('id')->on('users')->noDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
