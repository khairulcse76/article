<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 150)->unique();
            $table->string('slug', 250)->index();
            $table->integer('created_by')->unsigned()->index()->unllable();
            $table->integer('deleted_by')->unsigned()->index()->unllable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('created_by')->reference('id')->on('users')->noDelete('set null');
            $table->foreign('deleted_by')->reference('id')->on('users')->noDelete('set null');
        });
    }
    public function down()
    {
        Schema::dropIfExists('tags');
    }
}
