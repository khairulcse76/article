<?php
$factory->define(App\Profile::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'bio' => $faker->paragraphs(5),
        'web' => $faker->url,
        'facebook' => 'https://www.facebook.com/'.$faker->unique()->firstName,
        'github' => 'https://www.github.com/'.$faker->unique()->lastName,
        'remember_token' => str_random(10),
    ];
});