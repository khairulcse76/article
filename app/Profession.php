<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDelete;

class Profession extends Model
{
    use softDelete;

    protected $fillable=[
        'name',
    ];

    protected $dates = ['deleted_at'];

    public function users()
    {
        return $this->hasMany('App\User');
    }
}
