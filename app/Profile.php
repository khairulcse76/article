<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{

    protected $fillable = [
        'user_id', 'picture', 'bio',
        'web', 'facebook', 'github',
    ];
    protected $guarded = [
        'id', 'created_at', 'updated_at',
    ];

    public function users()
    {
        return $this->belongsTo('App\User');
    }
}
